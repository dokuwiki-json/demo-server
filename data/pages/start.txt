====== JSON database integrated into DokuWiki ======

With plugins described here you can build [[https://json.org/|JSON]] database inside dokuwiki pages. JSON (JavaScript Object Notation) is a lightweight data-interchange format. It is easy for humans to read and write. It is easy for machines to parse and generate. It is a [[wp>Document-oriented database]], so no SQL is used.

<json id=r1 path=recipe display=all>{
  "name": "Butter cookie",
  "type": "cookie",
  "Ingredients": {
    "eggs": "3",
    "flour": "500 g",
    "sugar": "350 g",
    "butter": "250 g"
  }
}
</json>

Above is a recipe for a %$recipe.name%. It has the following ingredients:
%$recipe.Ingredients{}%

There is much more. For further information see:
  * [[https://www.dokuwiki.org/plugin:json|JSON data plugin]]
    * [[json:json_definition_demo|JSON Data Definition Demo]]
    * [[json:json_usage_demo|JSON Data Usage Demo]]
    * [[json:benchmark|JSON Data Benchmark Testing]]
  * [[https://www.dokuwiki.org/plugin:jsoneditor|JSON editor plugin]]
    * [[jsoneditor:jsoneditor_demo|JSON editor demo]]
  * [[https://www.dokuwiki.org/plugin:jsontable|JSON table plugin]]
    * [[jsontable:jsontable_demo|JSON table demo]]
  * [[https://www.dokuwiki.org/plugin:jsongendoc|JSON generate document plugin]]
    * [[jsongendoc:jsongendoc_demo|JSON generate document demo]]
